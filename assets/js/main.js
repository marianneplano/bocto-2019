function setStyles(){

	var residents = document.querySelectorAll(".residents li");

	residents.forEach((resident, index) => {

		var letters = resident.innerHTML.split('');
		var array = [];
		var count = letters.length;

		if(index % 2){

			resident.style.color="rgb(100,75,10)";

		}

		if(count < 13){

			resident.classList.add('extend');

		}else if(count > 16){

			resident.classList.add('condens');

		}else{

			resident.classList.add('regular');

		}

		letters.forEach((letter) => {

			var span = (letter == " ") ? "<span>&nbsp;</span>"  : "<span class='notSpace'>"+letter+"</span>";

			array.push(span);

		});

		resident.innerHTML="<span class='inside'>"+array.join('')+"</span>";

	});

}

function setMedias(){

	

	var wrap = document.getElementById("media1");
	var medias = wrap.querySelectorAll("#media1 li");
	var h = wrap.parentElement.clientHeight/2;

	var half = Math.ceil(medias.length/2)-1;

	medias.forEach((media, index) => {

		if(window.innerWidth > 900){

			media.style.transform="rotate("+(20*(index%2))+"deg)";

			if(index > half){

				media.style.marginLeft=(((index-half)*12))+"vw";

			}else{

				media.style.marginLeft=(index*15)+"vw";

			}

		}else{

			media.style.marginLeft=(index*6)+"vw";
		}

	});

}

setStyles();
setMedias();

