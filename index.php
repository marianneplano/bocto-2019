<!DOCTYPE html>
<html>
<head>
	<meta lang="fr">
	<meta charset="utf-8">
	<title>BOCTO - Festival Art et Mouvement</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/css/main.css" title="" type="text/css">
	<link rel="stylesheet" href="assets/css/page.css" title="" type="text/css">
</head>
<body>
	<section class="center">
		<header>
			<h1>BOCTO</h1>
			<h2><span class="AM" >Art et<br>mouvement</span><span class="ED">3<sup>e</sup> édition</span></h2>
		</header>
		<ul id="media1">
			<li><span>Danse</span></li>
			<li><span>Performances</span></li>
			<li><span>Arts&nbsp;Visuels</span></li>
			<li><span>Art&nbsp;Vidéo</span></li>
			<li><span>Art&nbsp;Sonore</span></li>
			<li><span>Médias&nbsp;Interactifs</span></li>
			<li><span>Concerts</span></li>
		</ul>
		<ul id="year">
			<li><span>2</span></li>
			<li><span>0</span></li>
			<li><span>1</span></li>
			<li><span>9</span></li>
		</ul>
		<ul class="residents" id="resident-top">
			<li>Claire Forclaz</li>
			<li>Gabrielle Rossier</li>
			<li>Rémy Bender</li>
			<li>Frédéric Voeffray</li>
			<li>Maude Gyger</li>
			<li>Benoît Perrier</li>
			<li>Basile Richon</li>
			<li>Christel Voeffray</li>
			<li>Andrea Züllig</li>
			<li>Heiko Schätzle</li>
			<li>Dorine Aguerre</li>
			<li>Elorri Charriton</li>
			<li>Pauline Richon</li>
			<li>Babou Sanchez</li>
			<li>Marianne Plano</li>
			<li>Fa—Hsuan Chen</li>
			<li>Fanny Monier</li>
			<li>Juliette Le Monnyer</li>
			<li>Maxime Gourdon</li>
			<li>Patxi Andara</li>
			<li>Silvia Marson</li>
			<li>Antoine Gelgon</li>
			<li>Simon David</li>
			<li>Camille Lemille</li>
		</ul>
		<section class="info">
			<section class="right">
				<span class="EX">Exposition en<br>plein air</span>
				<br>
				<br>
				<div class="ephemeride">
					<div class="jour">SAMEDI</div>
					<div class="nume">20</div>
					<div class="mois"><span>J</span><span>U</span>ILL<span>E</span><span>T</span></div>
					<br>
					<div class="heure">12 h -> 20 h</div>
					<span class="verse"><br>Espace d’en Bas,&nbsp;Les Places, <i>Versegères</i><br></span> 
					<div class="raclette"><br>bar, restauration et&nbsp;raclette sur place</div>
					<div style="font-size: 24px;" ><br>AVEC LE SOUTIEN<br>DE LA COMMUNE DE BAGNES</div><br>
				</div>
			</section>
			<div class="text">
				<p>
					La résidence <strong>BOCTO</strong>, c’est une&nbsp;semaine <strong>(14 – 22 juillet 2019)</strong> de création artistique à Versegères dans le Val de Bagnes sur un site situé sur un terrain en plein air en dessous du village, aux Places d'En Bas. Cette résidence offre à&nbsp;des artistes émergents l’opportunité de poursuivre leur pratique dans un espace particulier et privilégie l’échange.
				</p>
				<p>
					<strong>24 artistes valaisans, suisses et d’ailleurs</strong> y sont invités à créer. Venant tous de disciplines artistiques différentes, ils créeront sur la thématique du lieu et du village de Versegères. Ces projets seront présentés en fin de résidence lors d’une exposition sous forme de parcours en plein air, ponctué de performances et spectacles, qui aura lieu le <strong>samedi 20 juillet 2019</strong>, date à laquelle vous êtes invités à venir découvrir les travaux réalisés durant la semaine et discuter autour d’un verre et d'une raclette dans une ambiance conviviale. Le groupe de musique «Voltige» ainsi que le joueur de disque Netillo Rojas accompagneront la fin de la journée.
				</p>
				<br>
				<br>
				<div class="map" style="float: left">
					<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=7.234947681427003%2C46.06118652839608%2C7.242028713226319%2C46.064599785632296&layer=mapnik&marker=46.062893183392205%2C7.23848819732666" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/?mlat=48.8290&mlon=2.3251#map=15/48.8290/2.3251">Afficher une carte plus grande</a></small>
				</div>
			<br>
			<br>
			<div class="contact" style="margin: 20px; float: left">
				<ul>
					<li>Espace d’en Bas, Les Places, Versegères.<br>-</li>
					<li><a href="https://www.instagram.com/residence.bocto/">Instagram</a> </li>
					<li><a href="https://www.facebook.com/events/2964488630244277/">Facebook</a> </li>
					<li><a href="mailto:name@email.com">collectif@facteur.org</a> </li>					
				</ul>
			</div>
			</div>			
		</section>
	</section>
	<div class="credit">design: <a href="http://luuse.io">Luuse</a></div>
	<script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>
